# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])


r1 = Role.create(name: "Admin")
r2 = Role.create(name: "Moder")
r3 = Role.create(name: "User")

u1 = User.create(name: "ADmin", email: "a@a.a", password: "987654321", role: r1)
u2 = User.create(name: "Moder", email: "m@m.m", password: "987654321", role: r2)
u3 = User.create(name: "User", email: "u@u.u", password: "987654321", role: r3)
